import mongoose from 'mongoose';

import products from './products';

function initDatabase() {
  const Product = mongoose.model('Product');

  return Product.findOne().lean()
    .then(product => product ? null : Product.create(products));
}

module.exports = initDatabase;
