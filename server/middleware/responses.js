import { ERROR_TYPES } from '../constants';

class ResponseFactory {
  constructor(res) { this.res = res; }

  notFoundResource(message, params) {
    const { type, code } = ERROR_TYPES.invalidRequestError;

    return this.errorResponse(type, code, message, params);
  }

  requestError(message, params) {
    const { type, code } = ERROR_TYPES.invalidParamError;

    return this.errorResponse(type, code, message, params);
  }

  serverError(message, params) {
    const { type, code } = ERROR_TYPES.internalError;

    return this.errorResponse(type, code, message, params);
  }

  success() {
    return this.res.status(200).json({});
  }

  errorResponse(type, code, message, params) {
    const response = { type, message };

    if (params && params.length) response.params = params;

    return this.res.status(code).json({ error: response });
  }
}

function responseMiddleware() {
  return (req, res, next) => {
    res.responses = new ResponseFactory(res);
    next();
  };
}

module.exports = responseMiddleware;
