import mongoose from 'mongoose';

import config   from '../../config';

const cartSchema = new mongoose.Schema({
  products: [{
    _id:      0,
    product:  { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
    id:       { type: Number, required: true, min: 1 },
    quantity: { type: Number, required: true, min: 0 }
  }]
}, {
  strict:     true,
  versionKey: false,
  timestamps: true,
  collection: 'carts'
});

cartSchema.index({ 'createdAt': 1 }, { expireAfterSeconds: config.get('cartTTL') });

cartSchema.options.toJSON = {
  transform: (doc, ret) => {
    delete ret._id;
    delete ret.updatedAt;
    delete ret.createdAt;
  }
};

module.exports.Cart = mongoose.model('Cart', cartSchema);
