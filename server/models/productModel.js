import mongoose      from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const productSchema = new mongoose.Schema({
  name:        { type: String, required: true },
  description: { type: String, required: true },
  price:       { type: Number, required: true, min: 0 }
}, {
  strict:       true,
  versionKey:   false,
  timestamps:   true,
  collection:   'products'
});

productSchema.options.toJSON = {
  transform: (doc, ret) => {
    delete ret._id;
    delete ret.updatedAt;
    delete ret.createdAt;
  }
};

productSchema.plugin(autoIncrement.plugin, {
  model:   'Product',
  field:   'id',
  startAt: 1
});

module.exports.Product = mongoose.model('Product', productSchema);
