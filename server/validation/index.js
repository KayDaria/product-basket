import validate    from 'validate.js';

function validateMiddleware(validationSchema, name = 'body') {
  return (req, res, next) => {
    const paths = {
      'body':   req.body,
      'params': req.params,
      'query':  req.query
    };

    validate.async(paths[name], validationSchema, { format: 'detailed' })
      .then(() => next())
      .catch(err => {
        const params = formatError(err);

        return res.responses.requestError('Invalid data parameters', params);
      });
  };
}

function formatError(validationErrors) {
  return validationErrors.map(err => ({
      code:    err.validator,
      message: err.error,
      name:    err.attribute
    })
  );
}

export default validateMiddleware;

