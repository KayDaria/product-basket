export const createProductSchema = {
  quantity: {
    presence: true,
    numericality: {
      onlyInteger:          true,
      noStrings:            true,
      greaterThanOrEqualTo: 1,
      lessThanOrEqualTo:    10
    }
  },
  product_id: {
    presence: true,
    numericality: {
      onlyInteger: true,
      noStrings:   true
    }
  }
};

export const deleteProductSchema = {
  product_id: {
    presence: true,
    numericality: {
      onlyInteger: true,
      greaterThanOrEqualTo: 1
    }
  }
};