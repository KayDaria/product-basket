module.exports = (app) => {
  app.use('/api/products',      require('./controllers/productController'));
  app.use('/api/cart',          require('./controllers/cartController'));
};