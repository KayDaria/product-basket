import winston from 'winston';
import fs      from 'fs';

const logDir = `${__dirname}/../logs`;

if (!fs.existsSync(logDir)) fs.mkdirSync(logDir);

const logger = winston.createLogger({
  level:            'debug',
  exitOnError:      false,
  handleExceptions: false,
  transports: [
    new winston.transports.File({
      filename: `${logDir}/app.log`,
      json:     true,
      colorize: false
    }),
    new winston.transports.Console({
      json:     false,
      colorize: true
    })
  ]
});

module.exports = logger;