import express  from 'express';
import mongoose from 'mongoose';

import validator from '../validation';
import {
  createProductSchema,
  deleteProductSchema
}                from '../validation/cartSchemas';

const router = express.Router();

const Cart    = mongoose.model('Cart');
const Product = mongoose.model('Product');

function getCart(req, res, next) {
  const sessionCartId  = req.session.cartId;
  const response = {
    data: {
      total_sum:      0,
      products_count: 0,
      products:       []
    }
  };

  return Promise.resolve(sessionCartId ? Cart.findOne({ _id: sessionCartId }).lean() : null)
    .then(cart => {
      if (!cart) return Cart.create({})
        .then((cart) => {
          req.session.cartId = cart._id;

          return res.json(response);
        });

      return Cart.aggregate([
            { $match: { "_id": mongoose.Types.ObjectId(sessionCartId) } },
            { $unwind: { path : "$products" } },
            {
              $lookup:
                {
                  from:         "products",
                  localField:   "products.product",
                  foreignField: "_id",
                  as:           "product"
                }
            },
            { $unwind: { path : "$product" } },
            {
              $project: {
                "_id":      0,
                "quantity": "$products.quantity",
                "id":       "$products.id",
                "price":    "$product.price"
              }
            },
            {
              $group: {
                "_id":            null,
                "total_sum":      { $sum: { $multiply: [ "$price", "$quantity" ] } },
                "products_count": { $sum: "$quantity" },
                "products":       { $push: {
                  "id": "$id",
                  "quantity": "$quantity",
                  "sum": { $multiply: [ "$price", "$quantity" ] }
                } }
              }
            },
            { $project: { "_id": 0 } }
        ]).exec()
        .then(response => res.json(response[ 0 ]));
    })
    .catch(err => next(err));
}

function addProduct(req, res, next) {
  const { product_id: productId, quantity } = req.body;
  const sessionCartId                       = req.session.cartId;

  return Product.findOne({ id: productId }).lean()
    .then(product => {
      if (!product) return res.responses.notFoundResource('Product not found');

      return Promise.resolve(sessionCartId ? Cart.findOne({ _id: sessionCartId }).lean() : null)
        .then(cart => {
          if (!cart) return Cart.create({ products: [ { product: product._id, id: productId, quantity } ] })
            .then((cart) => {
              req.session.cartId = cart._id;

              return res.responses.success()
            });

          const isProductAdded = cart.products.find(({ id }) => id === productId);

          const query = { _id: sessionCartId };
          let doc;

          if (isProductAdded) {
            query[ 'products.id' ] = productId;
            doc                    = { $inc: { 'products.$.quantity': quantity } };
          } else doc = { $push: { products: { product: product._id, id: product.id, quantity } } };

          return Cart.update(query, doc)
            .then(() => res.responses.success());
        });
    })
    .catch(err => next(err));
}

function deleteProduct(req, res, next) {
  const productId     = Number(req.params.product_id);
  const sessionCartId = req.session.cartId;

  return Product.findOne({ id: productId })
    .then(product => {
      if (!product) return res.responses.notFoundResource('Product not found');

      return Promise.resolve(sessionCartId ? Cart.findOne({ _id: sessionCartId }) : null)
        .then(cart => {
          if (!cart) return res.responses.success();

          const isProductAdded = cart.products.find(({ id }) => id === productId);

          if (!isProductAdded) return res.responses.notFoundResource('Deleted product not found');

          let savePromise = cart.save();
          if (isProductAdded.quantity <= 1) {
            savePromise = Cart.findByIdAndUpdate(cart._id, { $pull: { products: { id: productId }}});
          }
          else isProductAdded.quantity -= 1;

          return Promise.resolve(savePromise)
            .then(() => res.responses.success());
        });
    })
    .catch(err => next(err));
}

router.get('/', getCart);
router.post('/', validator(createProductSchema), addProduct);
router.delete('/:product_id([0-9]+)', validator(deleteProductSchema, 'params'), deleteProduct);

module.exports = router;