import express   from 'express';
import mongoose  from 'mongoose';

const router = express.Router();

const Product = mongoose.model('Product');

function getProducts(req, res, next) {
  return Product.find({}, '-_id -updatedAt -createdAt ').sort('id')
    .then(products => res.json(products))
    .catch((err) => next(err));
}

router.get('/',     getProducts);

module.exports = router;