export const ERROR_TYPES = {
  invalidParamError:   {
    type: 'invalid_param_error',
    code: 400
  },
  invalidRequestError: {
    type: 'invalid_request_error',
    code: 404
  },
  internalError: {
    type: 'internal_error',
    code: 500
  }
};