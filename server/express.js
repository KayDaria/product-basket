import http            from 'http';
import express         from 'express';
import bodyParser      from 'body-parser';
import cookieParser    from 'cookie-parser';
import cookieSession   from 'cookie-session';

import router          from './router';
import responses       from './middleware/responses';
import config          from '../config';

let logger;
let httpServer;

function start(mongooseConnection, _logger, port) {
  logger = _logger;

  let app = express();

  app.use(responses());

  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use((err, req, res, next) => {
    return res.responses.requestError('Invalid JSON string.');
  });

  const opt = config.get('cookie');

  app.use(cookieSession({
    keys:   [ opt.secretKey ],
    maxAge: opt.maxAge
  }));

  router(app);

  app.use((err, req, res, next) => {
    if (!err) return next();

    return res.responses.serverError('Unexpected Error in controller');
  });

  app.use('/api/*', (req, res) => {
    return res.responses.notFoundResource(`Unable to resolve the request "${req.originalUrl}".`);
  });

  httpServer = http.createServer(app);
  httpServer.listen(port, (err) => {
    return new Promise((resolve, reject) => {
      if (err) return reject(err);

      logger.debug(`Server started at ${port} port`);
      return resolve();
    });
  });
}

function stop() {
  return new Promise((resolve, reject) => {
    httpServer.close((err, result) => {
      return err ? reject(err) : resolve(result);
    });
  });
}

module.exports = {
  start,
  stop
};
