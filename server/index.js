import Promise     from 'bluebird';

import MongoClient from './mongoDB';
import logger      from './logger';
import config      from '../config';
import initDB      from './migration';
import server      from './express';

let mongoClient;

export function start() {
  logger.info(`Application loaded "${process.env.NODE_ENV || 'default'}" config`);

  process.on('uncaughtException', (err) => {
    logger.error({ source: 'uncaughtException', error: err.stack });

    process.exit(1);
  });

  process.on('unhandledRejection', (err) => {
    logger.error({ source: 'unhandledRejection', error: err.stack });

    process.exit(1);
  });

  const options = config.get('MongoDB');

  mongoClient = new MongoClient(logger);

  return Promise.resolve(mongoClient.connect(options.uri))
    .then(mongooseConnection => [
      mongooseConnection,
      options.autoApplyMigrations ? Promise.resolve(initDB()) : null
    ])
    .then(mongooseConnection => {
      server.start(mongooseConnection, logger, config.get('http').port);
      logger.info('Application started');
    })
    .catch(err => {
      logger.error({ source: 'uncaughtException', error: err.stack });

      process.exit(1);
    });
}

export function stop() {
  return Promise.all([
    server.stop(),
    mongoClient.disconnect()
  ])
    .then(() => logger.info('Application stopped'));
}

