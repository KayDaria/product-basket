const path          = require('path');
const convict       = require('convict');
const fs            = require('fs');

const conf = convict({
  env: {
    doc:       'The application environment.',
    format:    [ 'dev', 'production' ],
    'default': 'production',
    env:       'NODE_ENV',
    arg:       'node-env'
  },
  http: {
    port: {
      doc:       'Http listening port',
      format:    'port',
      'default': 3000,
      arg:       'http-port'
    }
  },
  MongoDB: {
    uri: {
      doc:       'Mongodb connection string',
      format:    String,
      'default': 'mongodb://localhost:27017/baskets'
    },
    autoApplyMigrations: {
      doc:       'Auto apply migrations',
      format:    Boolean,
      'default': true
    },
    dropDatabaseAlways: {
      format:    Boolean,
      'default': false
    },
    debug: {
      format:    Boolean,
      'default': false
    }
  },
  cookie: {
    secretKey: {
      doc:       'Key to use to sign & verify cookie values',
      format:    String,
      'default': 'fdfERR^$UGV'
    },
    maxAge: {
      doc:       'Number representing the milliseconds from Date.now() for expiry',
      format:    Number,
      'default': 60 * 60 * 1000
    }
  },
  cartTTL: {
    doc:       'Number representing the milliseconds from Date.now() for expiry of store carts',
    format:    Number,
    'default': 5 * 60 * 1000
  }
});

const filePath = path.resolve(__dirname, 'env', `${conf.get('env')}.json`);

if (fs.existsSync(filePath)) conf.loadFile(filePath);

conf.validate();

module.exports = conf;
