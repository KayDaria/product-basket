## «Корзина» магазина
  
Простое API “корзины” интернет-магазина для неавторизованных
пользователей.
  
### Technologies:   
Node.js, MongoDB 
  
### Quick start:  
* Скопировать репозиторий `git clone https://KayDaria@bitbucket.org/KayDaria/product-basket.git`
* Перейти в директорию проекта `cd product-basket`
* Установить зависимости `npm i`
* Команда `npm run start` поднимет сервер с переменной окружения
  - NODE_ENV=dev
